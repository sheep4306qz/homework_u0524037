
# coding: utf-8

# In[2]:


def mySort(objList, bigFirst):
    if bigFirst is True:
        objList.sort(reverse=True)
    else:
        objList.sort()
    return objList

if __name__ == "__main__":
    aList = [1, 5, 3, 7, 1, 0, 4]
    print(aList)
    aSortList = mySort(aList, True)
    print(aSortList)
    aSortList = mySort(aList, False)
    print(aSortList)

