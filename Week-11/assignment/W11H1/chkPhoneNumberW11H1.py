import uim2609
import sys
import re
uim2609.extra['student_ID'] = '0524037' 
uim2609.logger.info('UIM2609_Begin')
def chkPhoneNumer(input):
	countrynum=['+01','+44','+93','+54','+43','+61','+973','+880','+32','+975','+591','+55','+855','+237','+86','+1264','+1268','+297','+1441','1767','+1473','+1758','+57','+65','+243','+41','+49','+45','+20','+34','+503','+358','+679','+33','+995','+233','+30','+502','+967','+509','+504','+852','+91','+354','+62','+964','+353','+39','+1876','+962','+7','+254','+81','+82','+965','+352','+853','+389','+261','+60','+960','+52','+212','+47','+674','+64','+505','+234','+92','+507','+675','+351','+595','+40','+7','+250','+966','+381','+248','+94','+65','+249','+46','+66','+886','+676','+90','+256','+380','+971','+598','+998','+58','+967']
	input=sys.argv[1:]
	input=str(input)
	pattern=re.compile(r"(\+\d{1,4})(\-\d{4}\-\d{3}\-\d{3})")
	patternchk1=re.compile(r"(\+{2}\d{1,4})(\-\d{4}\-\d{3}\-\d{3})")
	patternchk2=re.compile(r"(\+\d{1,4})(\-\d{4}\-\d{3}\-\d{4})")
	try:
		chkinput=re.search(pattern,input).group()
		chkinputcountry=re.search(pattern,input).group(1)
		chkinputchk1=re.search(patternchk1,input)
		chkinputchk2=re.search(patternchk2,input)
		if chkinputchk1 or chkinputchk2:
			print('電話格式有誤')
			return False
		if chkinputcountry in countrynum:
			print('電話存在')
			return True
		else:
			print('錯誤原因:國碼有誤')
			return False
	except:
		if len(sys.argv[1:])==0:
			print('請輸入內容')
			return False
		else:
			print('找不到電話號碼或著輸入格式有誤')
			return False
		try:
			chkinputchkgroup1=re.search(patternchk,input).group(1)
			chkinputchkgroup4=re.search(patternchk,input).group(4)
			if chkinputchkgroup1 !=None or chkinputchkgroup4 !=None:
				print('找不到電話號碼或著輸入格式有誤1')
				return False
		except:
			print('找不到電話號碼或著輸入格式有誤')
			return False
	return chkinput
if __name__ == "__main__":
	result=chkPhoneNumer(input)
	print(result)
uim2609.logger.info('UIM2609_End')
