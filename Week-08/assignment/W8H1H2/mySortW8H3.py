
# coding: utf-8

# In[ ]:


import uim2609
import sys
uim2609.extra['student_ID'] = '0524037' 
uim2609.logger.info('UIM2609_Begin')
print = uim2609.print
def mySort(alist, rule):
    if rule is True:
        smaller = []
        bigger = []
        keylist = []
        datasort=[]
        if len(alist) <= 1:
            return alist
        else:
            key = alist[0] #第一個數為key值
            for i in alist:
                if i < key: #比key值小的數
                    smaller.append(i)
                elif i > key: #比key值大的數
                    bigger.append(i)
                else:
                    keylist.append(i)
        smaller = mySort(smaller,rule)
        bigger = mySort(bigger,rule)
        datasort=bigger + keylist + smaller
        return datasort
    else:
        smaller = []
        bigger = []
        keylist = []
        datasort=[]
        if len(alist) <= 1:
            return alist
        else:
            key = alist[0] #第一個數為key值
            for i in alist:
                if i < key: #比key值小的數
                    smaller.append(i)
                elif i > key: #比key值大的數
                    bigger.append(i)
                else:
                    keylist.append(i)
        smaller = mySort(smaller,rule)
        bigger = mySort(bigger,rule)
        datasort=smaller + keylist + bigger
        return datasort
if __name__ == "__main__":
    bool=True
    try:
        alist=sys.argv[1:-1]
        alist[:]=[x.strip(',') for x in alist]
        alist[:]=[int(x) for x in alist]
        print(alist)
        rule=sys.argv[-1]
        if rule=='True':
            bool=True
        elif rule=='False':
            bool=False
        aSortList=mySort(alist, bool)
        print(aSortList)
    except:
        alist=sys.argv[1]
        alist=alist.split(',')
        if alist[-1]=='':
             alist.remove('')
        else:
             alist=alist
        alist[:]=[int(x) for x in alist]
        print(alist)
        rule=sys.argv[-1]
        if rule=='True':
            bool=True
        elif rule=='False':
            bool=False
        aSortList=mySort(alist, bool)
        print(aSortList)
uim2609.logger.info('UIM2609_End')

